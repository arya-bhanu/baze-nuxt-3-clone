export default async (filters) => {
    const { data, error, pending } = await useSuitapiData(`/api/ideas`, {
            ...filters
        }
    )
    
    if (error.value) {
        throw createError({
            ...error.value,
            statusMessage: "Unable to fetch ideas"
        })
    }

    return {data, error, pending}
}