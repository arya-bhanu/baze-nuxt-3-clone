
export const useThemeStore = defineStore('theme', {
  state: () => ({
    darkTheme: false
  }),
  actions: {
    changeTheme() {
      this.darkTheme = !this.darkTheme
    }
  }
})

export const useToggleSidebar = defineStore('toggleSidebar', () => {
  const isToggled = ref(false)
  function toggle() {
    isToggled.value = !isToggled.value
  }
  return { isToggled, toggle }
})
