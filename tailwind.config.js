/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
    './app.vue'
  ],
  theme: {
    extend: {
      container: {
        padding: {
          DEFAULT: '2rem',
          sm: '3rem',
          lg: '5rem',
          xl: '7rem',
          '2xl': '9rem'
        }
      }
    }

    // fontFamily: {
    //   sans: ['Montserrat'],
    //   serif: ['Montserrat'],
    //   mono: ['Montserrat'],
    //   display: ['Montserrat'],
    //   body: ['Montserrat']
    // }
  },
  plugins: []
}
